//
//  RemoteLogger.h
//  Tamoco
//
//  Created by Sašo Sečnjak on 12/10/2016.
//  Copyright © 2016 Inova IT. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RemoteLogger : NSObject

+ (void) setLogLevel:(NSString* _Nullable)level;
+ (NSString* _Nullable) token;
+ (void) setup:(NSString* _Nonnull)key;
+ (void) log:(NSString* _Nonnull)message;
+ (void) logWarn:(NSString* _Nonnull)message;
+ (void) logErr:(NSString* _Nonnull)message;

@end
